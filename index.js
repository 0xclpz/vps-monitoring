var cmd = require('./cmd.js');

console.log(`Hostname: ${cmd.hostname()}`);
console.log(`Allumé depuis ${cmd.uptime()}`);

var ram = cmd.ram();

console.log(`RAM utilisée: ${ram.used}`);
console.log(`RAM libre: ${ram.free}`);

console.log(`IP externe: ${cmd.extIP()}`);
console.log(`Processus en cours: ${cmd.currProc()}`);
console.log(`Nombre d'utilisateurs connectés: ${cmd.users()}`);
console.log(`Heure système: ${cmd.systime()}`)
