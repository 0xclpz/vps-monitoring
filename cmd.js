var exec = require('child_process').execSync;
var StringDecoder = require('string_decoder').StringDecoder;
var exports = module.exports = {};

function cmd(args){
	var decoder = new StringDecoder('utf8');
	return decoder.write(exec(args));
};

exports.uptime = function getUptime(){
	var uptime =  cmd("uptime").match(/[0-9]{1,2}\:[0-9]{1,2}/g)[1];
	var heures = uptime.match(/[0-9]{1,2}/)[0];
	var minutes = uptime.match(/[0-9]{1,2}/g)[1];

	return `${heures} heures et ${minutes} minutes`;
}

exports.ram = function getRam(){
	var top = cmd('top -l 1 | grep "PhysMem"');
	var used = top.match(/[0-9]+\M/g)[0];
	var free = top.match(/[0-9]+\M/g)[2];

	return {
		used: used,
		free: free
	};
}

exports.hostname = function getHostname(){
	return cmd("hostname | xargs echo -n");
}

exports.extIP = function getExtIP(){
	return cmd("dig +short myip.opendns.com @resolver1.opendns.com | xargs echo -n");
}

//TODO: fix it
exports.intIP = function getIntIP(){
	return cmd("ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | head -n 1 | xargs echo -n")
}

exports.currProc = function getCurrProc(){
	return cmd("ps aux | tail -n +2 | wc -l").match(/[0-9]+/)[0];
}

exports.users = function getUsers(){
	return cmd("w | awk '{print $1}' | tail -n +3 | sort | uniq | wc -l").match(/[0-9]+/g)[0];
}

exports.systime = function getSystime(){
	return cmd("date | xargs echo -n");
}
